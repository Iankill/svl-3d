package svl

import (
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"regexp"
	"strconv"
)

//Test path permission
func LaunchProgram(algorithm string, container_width int, container_depth int, input_file string, ch chan int) {
	pathToProgram := "/usr/local/bin/3dBinPacking"
	//input_file := "/home/iankill/Documents/3d-bin-packing/tests/test_10000_1.xml"
	output_file := "output_file.xml"

	fmt.Println("Launch here")
	cmd := exec.Command(pathToProgram, algorithm, fmt.Sprintf("%d", container_width), fmt.Sprintf("%d", container_depth), "-f", input_file, "-o", output_file)
	var stdout bytes.Buffer
	cmd.Stdout = &stdout
	fmt.Println("Before run")
	err := cmd.Run()
	fmt.Println("After run")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Program terminated: ", stdout.String())

	rp := regexp.MustCompile("Bin height: [0-9]+")
	rpp := regexp.MustCompile("[0-9]+")
	wholeString := rp.FindString(stdout.String())
	binHeightString := rpp.FindString(wholeString)
	binHeight, _ := strconv.ParseInt(binHeightString, 10, 64)

	ch <- int(binHeight)
}
