package svl

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"os"
)

//Uppercase to export variables
type Cuboid struct {
	width  int
	height int
	depth  int
	x      int
	y      int
	z      int
}

type ContainerBase struct {
	width  int
	height int
	depth  int
}

type Problem struct {
	Container ContainerBase
	Cuboids   *[]Cuboid
}

//TODO Improve type to support JSON and move them to another file for package
type CuboidsXML struct {
	XMLName  xml.Name `xml:"cuboid" json:"-"`
	ClassID  string   `xml:"class_id,omitempty,attr" json:"-"`
	Tracking string   `xml:"tracking_level,omitempty,attr" json:"-"`
	Version  string   `xml:"version,omitempty,attr" json:"-"`
	Width    float32  `xml:"width" json:"width"`
	Height   float32  `xml:"height" json:"height"`
	Depth    float32  `xml:"depth" json:"depth"`
	X        float32  `xml:"x" json:"x"`
	Y        float32  `xml:"y" json:"y"`
	Z        float32  `xml:"z" json:"z"`
}

type BaseXML struct {
	XMLName xml.Name `xml:"base" json:"-"`
	Width   int      `xml:"width" json:"bin_width"`
	Height  int      `xml:"-" json:"bin_height"`
	Depth   int      `xml:"height" json:"bin_depth"`
}

//TODO Rename to Cuboids and add MaxHeight
type Boost struct {
	XMLName    xml.Name     `xml:"boost_serialization" json:"-"`
	Signature  string       `xml:"signature,attr" json:"-"`
	Version    int          `xml:"version,attr" json:"-"`
	Cuboids    []CuboidsXML `xml:"cuboid" json:"boxes"`
	Base       BaseXML      `xml:"base" json:"container"`
	Max_height int          `xml:"-" json:"max_height"`
	Algorithm  string       `xml:"-" json:"algorithm"`
}

func (c CuboidsXML) String() string {
	return fmt.Sprintf("\nWidth: %d\tDepth: %d\tHeight: %d\nX: %f\tY: %f\tZ: %f", c.Width, c.Depth, c.Height, c.X, c.Y, c.Z)
}

//TODO add FilePath and return structure
func ParseXML(filePath string, bin_height int) Boost {
	xmlFile, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Error opening file,", err)
	}
	defer xmlFile.Close()
	fmt.Println("Umarshalling...")

	var q Boost
	XMLdata, _ := ioutil.ReadAll(xmlFile)

	/*data := []byte(`<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
	<!DOCTYPE boost_serialization>
	<boost_serialization signature="serialization::archive" version="11">
	<cuboid class_id="0" tracking_level="0" version="0">
		<width>20</width>
		<height>30</height>
		<depth>10</depth>
		<x>10</x>
		<y>15</y>
		<z>5</z>
	</cuboid>
	<cuboid>
		<width>13</width>
		<height>14</height>
		<depth>12</depth>
		<x>6.5</x>
		<y>7.0</y>
		<z>16</z>
	</cuboid>
	<cuboid>
		<width>12</width>
		<height>13</height>
		<depth>11</depth>
		<x>6</x>
		<y>6.5</y>
		<z>27.5</z>
	</cuboid>
	<base class_id="1" tracking_level="0" version="0">
		<width>200</width>
		<height>200</height>
		<x>0</x>
		<y>0</y>
	</base>
	</boost_serialization>`)*/

	xml.Unmarshal(XMLdata, &q)
	fmt.Println("Len")
	fmt.Println(len(q.Cuboids))
	fmt.Println(q.Base.Width)
	for key, cuboid := range q.Cuboids {
		fmt.Println("MAIS LOOOL")
		fmt.Println(cuboid.String())
		q.Cuboids[key].X = float32(math.Floor(float64(cuboid.X - float32(cuboid.Width/2))))
		q.Cuboids[key].Y = float32(math.Floor(float64(cuboid.Y - float32(cuboid.Height/2))))
		q.Cuboids[key].Z = float32(math.Floor(float64(cuboid.Z - float32(cuboid.Depth/2))))

	}

	q.Base.Height = bin_height
	q.Max_height = bin_height
	return q
}

//TODO parameters and return value
//Structure to write XML
func JsonToXMLproblem(json Boost, filePath string) {
	c1 := Cuboid{10, 20, 30, 0, 0, 0}
	c2 := Cuboid{11, 12, 13, 0, 0, 0}
	c3 := Cuboid{12, 13, 14, 0, 0, 0}
	cont := ContainerBase{30, 30, 30}

	prob := Problem{}
	prob.Container = cont
	prob.Cuboids = &[]Cuboid{}
	*prob.Cuboids = append(*prob.Cuboids, c1, c2, c3)

	boost := Boost{Signature: "serialization::archive", Version: 10}
	fmt.Println(len(*prob.Cuboids))
	boost.Cuboids = make([]CuboidsXML, len(json.Cuboids))
	for index := range json.Cuboids {
		fmt.Println(index)

		cuboidToAdd := CuboidsXML{Width: (json.Cuboids)[index].Width,
			Height: (json.Cuboids)[index].Height,
			Depth:  (json.Cuboids)[index].Depth,
			X:      0,
			Y:      0,
			Z:      0}

		fmt.Println(cuboidToAdd)
		if index == 0 {
			cuboidToAdd.ClassID = "0"
			cuboidToAdd.Tracking = "0"
			cuboidToAdd.Version = "0"
		}

		boost.Cuboids[index] = cuboidToAdd
	}

	//filename := "/tmp/output.xml"

	file, _ := os.Create(filePath)

	xmlWriter := io.Writer(file)
	enc := xml.NewEncoder(xmlWriter)
	enc.Indent("  ", "    ")
	xmlHeader := `<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>` + "\n" +
		`<!DOCTYPE boost_serialization>` + "\n"
	xmlWriter.Write([]byte(xmlHeader))
	if err := enc.Encode(boost); err != nil {
		fmt.Println("Error, ", err)
	}

}

//TODO Read JSON to Structure
/*
{   "id": 1,
    "bin_width": 100,
    "bin_depth": 50,
    "boxes": {
                [   {width: 10,
                    depth: 10,
                    height:10},
                    {width: 15,
                    depth: 12,
                    height:5}
                ]
            }
}

*/

//TODO Write Structure to JSON
/*
{
    "boxes":[...] (With posXYZ and order to pack),
    bin_height: 43,
    bin_width: 100,
    bin_height: 50

}
*/
func writeJSON() {
	c1 := CuboidsXML{Width: 10, Height: 20, Depth: 30}
	c2 := CuboidsXML{Width: 11, Height: 12, Depth: 13}
	c3 := CuboidsXML{Width: 12, Height: 13, Depth: 14}

	boost := Boost{}
	boost.Cuboids = make([]CuboidsXML, 0)
	boost.Cuboids = append(boost.Cuboids, c1, c2, c3)

	filename := "output.json"
	file, _ := os.Create(filename)
	jsonWriter := io.Writer(file)
	enc := json.NewEncoder(jsonWriter)
	//b, _ := json.MarshalIndent(boost,"", "    ")

	if err := enc.Encode(boost); err != nil {
		fmt.Println("Error, ", err)
	}
}

func main() {
	fmt.Println("SVL")
	//parse()
	//jsonToXMLproblem()
	writeJSON()
}
